# !/bin/bash

# run 'sudo apt-get update' before you run this program

# This script
# 1.) Creates a ssh key
# 2.) Installs
#     i.) git, pip (for python), ansible


echo " **************************************** "
echo "          Starting setup process          "
echo " **************************************** "
echo "  "

# install git, pip (for python2 and for python3)
sudo apt-get install git python-pip python3-pip build-essential software-properties-common \
  python3-numpy python-dev python3-dev python3-setuptools libpq-dev sqlite3 -y
# sudo apt-add-repository ppa:ansible/ansible
# sudo apt-get update
# sudo apt-get install ansible -y

# install requirements before running setup.py
pip3 install -U -r requirements.txt
python3 setup.py develop

echo "  "
echo " **************************************** "
echo "            Ending setup process          "
echo " **************************************** "
