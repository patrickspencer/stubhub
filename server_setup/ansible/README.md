Requirements: Ubuntu 14.04

Install ansible:
```
apt-get update
apt-get install -y software-properties-common
apt-add-repository ppa:ansible/ansible
apt-get update
apt-get install ansible
```

Make folders:
```
mkdir /var/www/ 
cd /var/www/
git clone git@bitbucket.org:patrickspencer/stubhub.git
```

On a unix server run the following
```
cp /var/www/stubhub/ansible/development.ansible-hosts /etc/ansible/hosts
```

un ansible playbook
```
cd /var/www/stubhub/ansible  
ansible-playbook site.yml
```




