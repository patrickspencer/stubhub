apt-get update
apt-get install -y git vim software-properties-common
apt-add-repository ppa:ansible/ansible
apt-get update
apt-get install ansible
mkdir /var/www/
cd /var/www/
git clone git@bitbucket.org:patrickspencer/stubhub.git
cp /var/www/stubhub/ansible/development.ansible-hosts /etc/ansible/hosts
cd /var/www/stubhub/ansible
ansible-playbook site.yml

# add line for installing virtualenvwrapper and adding WORKON env variable to .bashrc


