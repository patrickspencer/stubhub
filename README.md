# About

A python app to compare ticket prices from Stubhub

# Installation

Requirements:  
python (2.7 or 3.3+)  
postgres database

Change directory into folder with the file setup.py

1. run `bash setup_server/setup.sh`
2. run `python setup.py develop`.
2. run `pip3 install -U -r requirements.txt `
3. copy `stubhub/settings.py.example` to `stubhub/settings.py` and change the
   variables in the file. You will have to follow the steps on the [Stubhub
   API getting started
   site](https://developer.stubhub.com/store/site/pages/guides.jag?type=gettingstarted)
   to setup your api credentials which you will enter into `settings.py`.


## Pulling data

1. run `python3 stubhub/populate/make_db.py` to make the database file.
2. run `python3 stubhub/populate/populate_events.py` to populate database with events
   from the Stubhub API.
3. run `python3 stubhub/populate/make_queue.py` to add items to the queue.
   Queue draws from the events in the database so run `populate_events.py`
   before `make_queue.py`.
4. run `python3 stubhub/populate/populate_listings.py` to populate database with
   listings. Draws listings from queue. Run `make_queue.py` before
   `populate_listings.py`.


# Database migrations

For migrations to work do the following:  
1. Copy alembic.ini.example to alembic.ini  
2. Change `sqlalchemy.url` in alembic.ini to the database uri,
postgres://User:password@localhost:5432/stubhub.
