# -*- coding: utf-8 -*-
"""
    dbtranslate
    ~~~~~~
    Running this program copies the contents of a postgres database to an sqlite database

"""

import os
from sqlalchemy import create_engine, inspect, exists
from sqlalchemy import Column, Integer, Date, Time, Float, String, ForeignKey
from sqlalchemy.orm import sessionmaker
from stubhub.models import engine, Event, ListingSnapshot, Listing

DATABASE = {
        'NAME': "stubhub",
        'USER': "Patrick",
        "PASSWORD": "password",
        "HOST": "localhost",
        "PORT": "5432",
    }

PG_DB_URI = 'postgres://%s:%s@%s:%s/%s' % (DATABASE['USER'],
        DATABASE['PASSWORD'],DATABASE['HOST'],DATABASE['PORT'],DATABASE['NAME'])

BASE_PATH = os.path.dirname(os.path.abspath(__file__))
SQLITE_DB_URI = "sqlite:///%s" % os.path.join(BASE_PATH,'db.sqlite3')

pg_engine = create_engine(PG_DB_URI, echo=True)
pg_session = sessionmaker(bind=pg_engine)
PGSession = pg_session()

sqlite_engine = create_engine(SQLITE_DB_URI, echo=True)
sqlite_session = sessionmaker(bind=sqlite_engine)
SLSession = sqlite_session()

# events = PGSession.query(Event).all()
# for event in events:
#     event_entry = Event(
#             id=event.id,
#             stubhub_id=event.stubhub_id,
#             is_active=event.is_active,
#             team_primary=event.team_primary,
#             team_secondary=event.team_secondary,
#             groupings=event.groupings,
#             city=event.city,
#             state=event.state,
#             url=event.url,
#             datetime=event.datetime,
#             )

#     if not SLSession.query(Event).filter_by(id=event_entry.id).count():
#         SLSession.add(event_entry)

# listing_snapshots = PGSession.query(ListingSnapshot).all()
# for snapshot in listing_snapshots:
#     snapshot_entry = ListingSnapshot(
#             event_id=snapshot.event_id,
#             datetime_accessed=snapshot.datetime_accessed,
#             )
#
#     results = SLSession.query(ListingSnapshot)\
#             .filter(ListingSnapshot.event_id==snapshot.event_id)\
#             .filter(ListingSnapshot.datetime_accessed==snapshot.datetime_accessed).first()
#
#     # if the row doesn't exists add it
#     if not results:
#         SLSession.add(snapshot_entry)

listings = PGSession.query(Listing).all()
for listing in listings:
    snapshot_entry = Listing(
            listing_snapshot_id = listing.listing_snapshot_id,
            order = listing.order,
            stubhub_id = listing.stubhub_id,
            datetime_accessed = listing.datetime_accessed,
            price = listing.price,
            section_name = listing.section_name,
            row = listing.row,
            seat_numbers = listing.seat_numbers,
            zone_name = listing.zone_name,
            split_vector = listing.split_vector,
            quantity = listing.quantity
            )

    results = SLSession.query(Listing)\
            .filter(Listing.listing_snapshot_id==listing.listing_snapshot_id)\
            .filter(Listing.stubhub_id==listing.stubhub_id)\
            .filter(Listing.datetime_accessed==listing.datetime_accessed)\
            .filter(Listing.order==listing.order).first()

    # if the row doesn't exists add it
    if not results:
        SLSession.add(snapshot_entry)

SLSession.commit()
