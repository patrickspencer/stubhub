# -*- coding: utf-8 -*-
"""
    models
    ~~~~~~
    Model definitions and class methods
"""

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, Boolean, DateTime, \
        String, ForeignKey, Float
from sqlalchemy.orm import relationship
from sqlalchemy import create_engine, inspect, desc, exists
from sqlalchemy.orm import sessionmaker
from stubhub import settings

engine = create_engine(settings.DATABASE_URI, echo=False)
Session = sessionmaker(bind=engine)
dbsession = Session()

Base = declarative_base()

class Event(Base):
    __tablename__ = 'events'

    id             = Column(Integer, primary_key=True)
    stubhub_id     = Column(Integer, unique=True)
    is_active      = Column(Boolean, unique=False, default=True)
    team_primary   = Column(String)
    team_secondary = Column(String)
    groupings      = Column(String)
    city           = Column(String)
    state          = Column(String)
    url            = Column(String)
    datetime       = Column(DateTime(timezone=True))

    def __repr__(self):
       return "<Event(name='%s v %s [%s]')>" % (self.team_primary, self.team_secondary, self.datetime)

class ListingSnapshot(Base):
    __tablename__ = 'listing_snapshots'

    id                = Column(Integer, primary_key=True)
    event_id          = Column(Integer, ForeignKey(Event.id))
    datetime_accessed = Column(DateTime(timezone=True), index=True)

    def __repr__(self):
       return "<ListingSnapshot(event_id='%s',current_date='%s',current_time='%s')>" % (self.event_id, self.current_date, self.current_time)

class Listing(Base):
    __tablename__ = 'listings'

    id                  = Column(Integer, primary_key=True)
    listing_snapshot_id = Column(Integer, ForeignKey(ListingSnapshot.id))
    order               = Column(Integer)
    stubhub_id          = Column(Integer, index=True)
    datetime_accessed   = Column(DateTime(timezone=True), index=True)
    price               = Column(Float)
    section_name        = Column(String, nullable=True)
    row                 = Column(String, nullable=True)
    seat_numbers        = Column(String, nullable=True)
    zone_name           = Column(String, nullable=True)
    split_vector        = Column(String, nullable=True)
    quantity            = Column(Integer, nullable=True)

    def __repr__(self):
        return "<ListingPrice(listing_snapshot_id='%s',order='%s',price='%s',section_name='%s,\
        'row='%s','seat_numbers='%s','zone_name='%s')>" % (self.listing_snapshot_id, self.order,
                self.price, self.section_name, self.row, self.seat_numbers, self.zone_name)

    @property
    def serialize(self):
       """
       Return object data in easily serializeable format
       """
       return {
           'listing_snapshot_id': self.listing_snapshot_id,
           'stubhub_id': self.stubhub_id,
           'price': self.price,
           'section_name': self.section_name,
           'row': self.row,
           'seat_numbers': self.seat_numbers,
           'zone_name': self.zone_name,
           'split_vector': self.split_vector,
           'quantity': self.quanitity,
       }

class QueueItem(Base):
    __tablename__ = 'queue_items'

    id             = Column(Integer, primary_key=True)
    stubhub_id     = Column(Integer)
    status         = Column(Integer, default=0)
    datetime_added = Column(DateTime)

    def __repr__(self):
        return "<QueueItem(id='%s',stubhub_id='%s',status='%s',datetime_added='%s')>" %\
                (self.stubhub_id,self.status,self.datetime_added)
