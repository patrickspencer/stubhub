import json
import requests
from stubhub import auth, settings

# Send an HTTP message to log in our account on the Stubhub servers
auth.send_auth_post()

# Create http headers so each request has our name on it
headers = {
    'Authorization': 'Bearer %s' % settings.APPLICATION_TOKEN,
    'Accept': 'application/json',
    'Accept-Encoding': 'application/json',
    }

BASE_URL = 'https://api.stubhub.com/'

def search_events(query, groupingName, start=1):
    """
    Returns a requests.Response object associated to querying stubhub
    The json part can be accessed by the .json() method i.e.
    search_events

        event_response = apiqueries.search_events(query='Golden State Warriors',
                                                  groupingName='NBA Regular')
        json = event_response.json()
        events = json['events']

    Api docs url: https://developer.stubhub.com/store/site/pages/doc-viewer.jag?category=Search&api=EventSearchAPIv2&endpoint=searchforeventsv2&version=v2

    :param query: string, e.g. "Orlando Magic"
    :param groupingName: string, e.g. "NBA Regular"
    :param start: integer, the index of the first event that will be returned.
    Indices are 0-based.
    """
    api_resource = 'search/catalog/events/v2'
    params = {
        'minAvailableTickets': 1,
        'limit': 500,
        'status': 'Active',
        'q': query,
        'groupingName': groupingName,
        'start': start,
    }

    # create an url like
    # https://api.stubhub.com/search/catalog/events/v2?q=query\
    #     &minAvailableTickets=1&limit=500&status=Active&groupingName=groupingName&start=start

    url = BASE_URL + api_resource
    return requests.get(url,headers=headers, params=params)

def get_event_by_id(event_id):
    """
    Returns a requests.Response object associated to querying stubhub for an
    event using the event_id
    The json part can be accessed by the .json() method i.e.

        event_response = apiqueries.get_event_by_id(9371236)
        json = event_response.json()
        events = json['events']

    Api docs url: https://developer.stubhub.com/store/site/pages/doc-viewer.jag?category=Catalog&api=EventsAPI&endpoint=getinfoaboutanevent&version=v2

    :param event_id: 7 digit integer. You can get this by looking up an event
                     on stubhub's website and getting this number from the url
    """
    api_resource = 'catalog/events/v2/'
    params = str(event_id)
    url = BASE_URL + api_resource + params
    return requests.get(url,headers=headers,params=params)

def get_listings_by_id(event_id):
    """
    Return a listings for an event.
    Returns a requests.Response object associated to querying stubhub for
    all listings associated to that event_id

    The json part can be accessed by the .json() method i.e.
    search_events

        event_response = apiqueries.get_listings_by_id(9371236)
        json = event_response.json()
        events = json['events']

    Api docs url: https://developer.stubhub.com/store/site/pages/doc-viewer.jag?category=Search&api=InventorySearchAPI&endpoint=searchinventory&version=v1

    :param event_id: 7 digit integer. You can get this by looking up an event
                     on stubhub's website and getting this numner from the url
    """
    api_resource = 'search/inventory/v1'
    # params = "?eventID=%d&pricingSummary=true&sort=currentPrice asc&quantity=>2" % event_id
    params = {
        'eventID': event_id,
        'pricingSummary': 'true',
        'sort': 'currentPrice asc',
        'quantity': '>2',
    }
    url = BASE_URL + api_resource
    return requests.get(url,headers=headers,params=params)
