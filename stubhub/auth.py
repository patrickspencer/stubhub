import sys
from stubhub import settings
import base64
import requests

EMAIL = settings.EMAIL
PASSWORD = settings.PASSWORD
APPLICATION_TOKEN = settings.APPLICATION_TOKEN
CONSUMER_KEY = settings.CONSUMER_KEY
CONSUMER_SECRET = settings.CONSUMER_SECRET

if (sys.version_info > (3, 0)):
    consumer_info = bytes("%s:%s" % (CONSUMER_KEY, CONSUMER_SECRET), 'UTF-8')
else:
    consumer_info = "%s:%s" % (CONSUMER_KEY, CONSUMER_SECRET)

base_auth_token = base64.b64encode(consumer_info)

headers = {
    'Content-type': 'application/x-www-form-urlencoded',
    'Authorization': 'Basic %s' % base_auth_token.decode("UTF-8")
    }

data = {
    'grant_type': 'password',
    'username': EMAIL,
    'password': PASSWORD,
    'scope': 'PRODUCTION'
}

data_string = 'grant_type=%s&username=%s&password=%s&scope=%s' % (data['grant_type'],data['username'],data['password'],data['scope'])

url = 'https://api.stubhub.com/login'
def send_auth_post():
    r = requests.post(url, data=data, headers=headers)

