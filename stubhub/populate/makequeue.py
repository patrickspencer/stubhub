import pytz
import pprint
import datetime
from stubhub import apiqueries
from stubhub.models import dbsession, Event, QueueItem
from stubhub.helpers.events import add_events_to_db
from stubhub.helpers.timezones import as_utc
from stubhub.helpers.listings import deactivate_event

pp = pprint.PrettyPrinter(indent=4)

event_results = dbsession.query(Event.id, Event.stubhub_id, Event.datetime)\
        .filter(Event.is_active == True).all()

for event in event_results:
    timenow = datetime.datetime.utcnow().replace(tzinfo=pytz.utc)
    # status is one of the following:
    #     0 = not started
    #     1 = completed
    queue_item = QueueItem(
        stubhub_id = event.stubhub_id,
        status = 0,
        datetime_added = timenow
        )

    results = dbsession.query(QueueItem)\
            .filter(QueueItem.stubhub_id==event.stubhub_id).first()

    if not results:
        dbsession.add(queue_item)

dbsession.commit()
