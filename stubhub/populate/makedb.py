from stubhub import models
from sqlalchemy import create_engine, inspect, desc, exists
from sqlalchemy.orm import sessionmaker
from stubhub import settings


engine = create_engine(settings.DATABASE_URI, echo=True)
session = sessionmaker(bind=engine)
Session = session()

#this next line creates the database if it doesn't exists already
models.Event.metadata.create_all(engine)

