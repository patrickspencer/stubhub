from flask import Blueprint, render_template
from stubhub.models import Event, dbsession
from stubhub.helpers.events import get_event_snapshots
main = Blueprint('main', __name__, url_prefix='/')

@main.route("/")
def index():
    events = dbsession.query(Event).all()
    return render_template('index.jinja2', events=events)

@main.route("event/<int:stubhub_id>/")
def event(stubhub_id):
    snapshot = get_event_snapshots(stubhub_id)
    return render_template('event.jinja2', snapshot=snapshot)
