import os
import time
import numpy
import datetime
from stubhub import apiqueries
from stubhub.models import Event, ListingSnapshot, Listing, dbsession
from stubhub.helpers import timezones
from stubhub.settings import LOGS_DIR
from stubhub.helpers.timezones import utc_to_local
from sqlalchemy import update

def get_event_id(query_results):
    """
    Return the event id from a json response object received by querying the
    Stubhub api for a listing by the event id. It seems like you should alread
    have the event id since that's how you got the query response in the first
    place but this is for when you only have the query response to work with.

    :param query_results: what you get with the following:

        response = apiqueries.get_listings_by_id(stubhub_event_id)
        query_results = response.json()
    """
    return query_results['eventId']

def deactivate_event(stubhub_event_id):
    dbsession.query(Event).filter(Event.stubhub_id == stubhub_event_id)\
            .update({Event.is_active: False})
    dbsession.commit()

def get_utc_date(event):
    """
    Return the UTC date of the event in the form 2016-04-14T17:30:00+0000
    use event = apiqueries.get_event_by_id(event_id).json()
    """
    return event['eventDateUTC']

def get_single_listing_price(listing):
    """
    Returns the price of a single listing. When you use
    apiqueries.get_listings_by_id(event_id).json() it gives you a list of listings.
    listings it returns something like this:

    {   "deliveryTypeSummary": [],
        "errors": None,
        "eventId": 9371267,
        "listing": [   {   "currentPrice": {"amount": 18.59, "currency": "USD"},
                           "deliveryFee": None,
                           "deliveryTypeList": [1]
                           ...
                       },
                       {   "currentPrice": {"amount": 25.61, "currency": "USD"},
                           "deliveryFee": None,
                           "deliveryTypeList": [2],
                           ...
                       },
                       ...(more listings)...,
        "listingAttributeCategorySummary": [],
        "mapDisplayType": "section",
        "maxQuantity": 20,
        "minQuantity": 1,
        "pricingSummary": {   "averageTicketPrice": 168.89204225352108,
                              "maxTicketPrice": 1172.5,
                              "medianTicketPrice": None,
                              "minTicketPrice": 18.59,
                              "name": "TICKET_PRICE",
                              "totalListings": 142},
        "rows": 100,
        "section_stats": None,
        "start": 0,
        "totalListings": 142,
        "totalTickets": 672,
        "zone_stats": None}
    }

    We are taking in a single listings out of the "listings" entry and then
    getting the price for that listing
    """
    return listing['currentPrice']['amount']

def get_split_vector(listing):
    return listing['splitVector']

def pare_down_list(query_results):
    """
    Returns a list of single listings. Only include listings where 2 is in the
    splitVector. We also only need the first (lowest priced) 10 tickets.

    :param query_results: what you get from

        response = apiqueries.get_listings_by_id(stubhub_event_id)
        query_results = response.json()

    It's a dictionary with a list of single listings in the ['listing'] attribute
    """
    list = []
    counter = 0
    for listing in query_results['listing']:
        if 2 in get_split_vector(listing):
            list.append(listing)
            counter += 1
        if counter == 10:
            break
    return list

def get_listing_prices(listings_list):
    """
    Return a list of ticket prices given a list of single listings

    :param listings_list: This is not what you get from

        apiqueries.get_listings_by_id(event_id).json().

    It's what you get if you do the following:

        query_results = apiqueries.get_listings_by_id(event_id).json()
        listings_list = listings_list['listing']

    i.e. it doesn't contain the extra metadata of a full listings_list like
    eventID.
    """
    list = []
    for listing in listings_list:
        list.append(get_single_listing_price(listing))
    return list

def objectify_listing_snapshot(event_id):
    """
    Returns a new ListingSnapshot object that we can use to insert
    a listing snapshot into the database.

    :param event_id: Stubhub 7 digit event id
    """
    db_query_results = dbsession.query(Event).filter(Event.stubhub_id == event_id)\
                    .first()
    listing_snapshot = ListingSnapshot(
            event_id = db_query_results.id,
            datetime_accessed = datetime.datetime.utcnow().replace(microsecond=0),
            )
    return listing_snapshot

def get_price(listing):
    """
    :param listing: An element of the list query_results['listing']
    where
    query_results = apiqueries.get_listings_by_id(event_id).json()
    """
    return listing['currentPrice']['amount']

def get_section_name(listing):
    """
    :param listing: An element of the list query_results['listing']
    where

        query_results = apiqueries.get_listings_by_id(event_id).json()
    """
    return listing['sectionName']

def get_row(listing):
    """
    :param listing: An element of the list query_results['listing']
    where

        query_results = apiqueries.get_listings_by_id(event_id).json()
    """
    return listing['row']

def get_seat_numbers(listing):
    """
    :param listing: An element of the list query_results['listing']
    where

         query_results = apiqueries.get_listings_by_id(event_id).json()
    """
    return listing['seatNumbers']

def get_zone_name(listing):
    """
    :param listing: An element of the list query_results['listing']
    where

        query_results = apiqueries.get_listings_by_id(event_id).json()
    """
    return listing['zoneName']

def get_quantity(listing):
    """
    :param listing: An element of the list query_results['listing']
    where

        query_results = apiqueries.get_listings_by_id(event_id).json()
    """
    return listing['quantity']

def get_stubhub_id(query_results):
    """
    :param query_results: what you get when yo do the following

        query_results = apiqueries.get_listings_by_id(event_id).json()
    """
    return query_results['eventId']

def add_listings_to_db(query_results):
    """
    Add listings to the database gathered from query results. Also add the
    lowest n prices to the database associated to each listing where the number
    n is determined by the function pare_down_list.

    Returns false if the query_result is an event whose eventId is None, which means
    Stubhub returned an empty event

    :param query_results: What you get by the following:

        response = apiqueries.get_listings_by_id(event_id)
        query_results = response.json()
    """
    if query_results['eventId'] is None:
        return False

    listing_snapshot = objectify_listing_snapshot(query_results['eventId'])
    dbsession.add(listing_snapshot)
    dbsession.flush()
    dbsession.commit()

    listings = pare_down_list(query_results)
    counter = 1
    for listing in listings:
        dbsession.add(Listing(
                        listing_snapshot_id = listing_snapshot.id,
                        order = counter,
                        datetime_accessed = datetime.datetime.utcnow().replace(microsecond=0),
                        stubhub_id = get_stubhub_id(query_results),
                        price = get_price(listing),
                        section_name = get_section_name(listing),
                        row = get_row(listing),
                        seat_numbers = get_seat_numbers(listing),
                        zone_name = get_zone_name(listing),
                        split_vector = str(get_split_vector(listing)).strip('[]'),
                        quantity = get_quantity(listing)
                        )
                   )
        counter += 1
    dbsession.commit()

def get_listings_by_date(date_object):
    return dbsession.query(Listing.stubhub_id, Listing.datetime_accessed) \
               .filter(Listing.stubhub_id == event_id) \
               .filter(cast(Listing.datetime_accessed, DATE) == date_object) \
               .all()

def get_snapshot_id_list(listing_snapshot_results):
    """
    make a list of snapshot ids so we can get the listings associated to each
    snapshot
    """
    list = []
    for snapshot in listing_snapshot_results:
        list.append(snapshot.id)
    return list

def get_snapshot_date(snapshot_object):
    return snapshot_object.datetime_accessed

def get_listings_from_snapshot_id(listing_results,snapshot_id):
    """
    Returns a list of listings. This is like running
        listings_results.filter(snapshot_id == snapshot_id)
    """
    l = []
    for listing in listing_results:
        if snapshot_id == listing.listing_snapshot_id:
            l.append(listing)
    return l

def format_date(datetime_obj):
    return datetime_obj.strftime("%Y-%m-%d %H:%M:%S %Z")

def get_snapshot_from_id(listing_snapshot_results,snapshot_id):
    """

    :param listing_snapshot_results: what you get when you run

        listing_snapshot_results = session.query(ListingSnapshot) \
              .filter(ListingSnapshot.event_id == event_results.id).all()
    """
    for listing in listing_snapshot_results:
        if listing.id == snapshot_id:
            return listing

# get all listings from a query result from the snapshot id. This
# is analagous to geting the listings with a certain datetime.
def make_snapshot_data_dict(snapshot_results,listing_results,snapshot_id):
    """
    Return a dict of snaphot data

    :param snapshot_results: what you get when you run

        snapshot_results = session.query(ListingSnapshot) \
                          .filter(ListingSnapshot.event_id == event_results.id).all()

    :param listing_results: what you get when you run

        listing_results = session.query(Listing) \
                  .filter(Listing.listing_snapshot_id.in_(id_list))
    """
    # get date from snapshot id
    date = get_snapshot_from_id(snapshot_results,snapshot_id)\
            .datetime_accessed

    # get set of listings from listings query whose snapshot_id is given
    listings = get_listings_from_snapshot_id(listing_results,snapshot_id)

    dict = {}
    snapshot_datetime = timezones.utc_to_local(date)\
            .strftime("%Y-%m-%d %H:%M %Z")
    dict['datetime'] = snapshot_datetime
    dict['snapshot_id'] = snapshot_id
    price_list = []
    for listing in listings:
        price_list.append(listing.price)
    dict['listing_prices'] = price_list
    dict['avg_price'] = numpy.mean(price_list)

    # each listing is a Listing object which belongs to the snapshot listing
    # for listing in listings:
    #     listing_dict = {}
    #     listing_dict['zone_name'] = listing.zone_name

    return dict

def write_string_to_logs(string):
    """
    Finds the correct log file name e.g. stubhub_log_2016-01-24.txt and appends
    the variable string to the end of that file
    """
    if not os.path.exists(LOGS_DIR):
        os.makedirs(LOGS_DIR)
    local_date = utc_to_local(datetime.datetime.utcnow()).strftime("%Y-%m-%d")
    log_file_name = "stubhub_log_%s.txt" % local_date
    log_file_full_path = os.path.join(LOGS_DIR,log_file_name)
    with open(log_file_full_path, "a") as logfile:
        logfile.write(string)

def add_listings_to_db_and_log(stubhub_event_id,optional_string=''):
    """
    A wrapper aroung the add_listings_to_db method which calls the function
    add_listings_to_db and also creates a status string which we get use to log
    the action in a separate file

    :param optional_string: this is an optional string that can be intserted
    into the status string at runtime. The intent is to add a line like "item 200
    out of 450. About 1 hour to go"
    """
    query_result = dbsession.query(Event).filter(Event.stubhub_id == stubhub_event_id).first()
    event_name = "%s vs %s" % (query_result.team_primary,query_result.team_secondary)
    location = "%s, %s" % (query_result.city,query_result.state)
    event_datetime = utc_to_local(query_result.datetime)
    event_date = event_datetime.strftime('%a, %b %d, %Y')
    event_time = event_datetime.strftime('%H:%M %Z')
    event_name += " in %s, %s %s" % (location,event_date,event_time)
    local_datetime = utc_to_local(datetime.datetime.utcnow()).strftime("%Y-%m-%d %H:%M:%S %Z")
    response = apiqueries.get_listings_by_id(stubhub_event_id)
    query_results = response.json()

    # If an event has already taken place or it has been cancelled then stubhub will return
    # an empty event on request. We have to check for that.
    if query_results['eventId'] is not None:
        add_listings_to_db(query_results)
        status_code = response.status_code
        action_string = "SUCCESS - Recieved event data. Inserted listing into database."
        log_string = "%s -%s get listing snapshot - %s - stubhub id: %s - %s"\
                % (local_datetime,optional_string,action_string,stubhub_event_id,event_name)
    else:
        status_code = response.status_code
        action_string = "ERROR - Recieved empty event from stubhub. Can not put data into database."
        log_string = "%s - get listing snapshot - %s - stubhub id: %s - %s"\
                % (local_datetime,action_string,stubhub_event_id,event_name)
        # deactivate an event so the next time we pull all the listings we
        # don't waste time on empty events
        deactivate_event(stubhub_event_id)
    print(log_string)
    write_string_to_logs(log_string)
