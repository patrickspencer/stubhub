from stubhub import apiqueries
from sqlalchemy import exists
from stubhub.models import Event, Listing, ListingSnapshot, dbsession
from stubhub.helpers.listings import get_listing_prices, pare_down_list, \
    add_listings_to_db, make_snapshot_data_dict, get_snapshot_id_list
from stubhub.helpers import timezones
import datetime

def get_teams(event):
    """
    returns a dictionary that looks like this:
    {
        'team_primary': 'Charlotte Hornets'
        'team_secondary': 'Orlando Magic'
    }

    :param event: a json dictionary of event information gotten by
    querying stubhub. This is what you get when you do the following:

        stubhub_response = apiqueries.search_events(query='Timberwolves',
            groupingName='NBA Regular')
        events = stubhub_response.json()['events']

    Note: I'm note sure if all events given by Stubhub will have the names
    act_primary and act_secondary. Maybe this is only for NBA games.
    """
    dict = {}
    for attribute in event['attributes']:
        if attribute['name'] == 'act_primary':
           dict['team_primary'] = attribute['value']
        if attribute['name'] == 'act_secondary':
           dict['team_secondary'] = attribute['value']
    return dict

def get_utc_date(event):
    """
    return the UTC date of the event in the form 2016-04-14T17:30:00+0000

    :param event: a json dictionary of event information gotten by
    querying stubhub. This is what you get when you do the following:

        stubhub_response = apiqueries.search_events(query='Timberwolves',
            groupingName='NBA Regular')
        events = stubhub_response.json()['events']
    """
    return datetime.datetime.strptime(event['dateUTC'],'%Y-%m-%dT%H:%M:%S%z')

def get_time(utc_date):
    """
    return the time portion of 2016-04-14T17:30:00+0000 i.e. it would
    return 17:30:00+0000

    :param date: a string of the form 2016-04-14T17:30:00+0000
    """
    return utc_date.split('T')[1]

def get_stubhub_id(event):
    """
    return stubhub id of the event

    :param event: a json dictionary of event information gotten by
    querying stubhub. This is what you get when you do the following:

        stubhub_response = apiqueries.search_events(query='Timberwolves',
            groupingName='NBA Regular')
        events = stubhub_response.json()['events']
    """
    return event['id']

def get_url(event):
    """
    :param event: a json dictionary of event information gotten by
    querying stubhub. This is what you get when you do the following:

        stubhub_response = apiqueries.search_events(query='Timberwolves',
            groupingName='NBA Regular')
        events = stubhub_response.json()['events']
    """
    return event['eventInfoUrl']

def get_city(event):
    """
    :param event: a json dictionary of event information gotten by
    querying stubhub. This is what you get when you do the following:

        stubhub_response = apiqueries.search_events(query='Timberwolves',
            groupingName='NBA Regular')
        events = stubhub_response.json()['events']
    """
    return event['venue']['city']

def get_state(event):
    """
    :param event: a json dictionary of event information gotten by
    querying stubhub. This is what you get when you do the following:

        stubhub_response = apiqueries.search_events(query='Timberwolves',
            groupingName='NBA Regular')
        events = stubhub_response.json()['events']
    """
    return event['venue']['state']

def get_groupings(event):
    """
    Return a list of groupings for the event that looks like
    ['NBA Tickets', '2015 NBA Regular Season Tickets']

    :param event: a json dictionary of event information gotten by
    querying stubhub. This is what you get when you do the following:

        stubhub_response = apiqueries.search_events(query='Timberwolves',
            groupingName='NBA Regular')
        events = stubhub_response.json()['events']
    """
    list = []
    for grouping in event['groupings']:
        list.append(grouping['name'])
    return list

def event_exists(stubhub_id):
    """
    Return True if event with stubhub_id exists and False otherwise
    """
    (ret, ), = dbsession.query(exists().where(Event.stubhub_id == stubhub_id))
    return ret

def objectify_event(event):
    """
    Returns an models.Event object

    :param event: a json dictionary of event information gotten by
    querying stubhub. This is what you get when you do the following:

        stubhub_response = apiqueries.search_events(query='Timberwolves',
            groupingName='NBA Regular')
        events = stubhub_response.json()['events']
    """

    return Event(
        stubhub_id = get_stubhub_id(event),
        team_primary = get_teams(event)['team_primary'],
        team_secondary = get_teams(event)['team_secondary'],
        groupings = ', '.join(get_groupings(event)),
        city = get_city(event),
        state = get_state(event),
        url = get_url(event),
        datetime = get_utc_date(event),
        )

def add_events_to_db(events):
    """
    :params events: A list of events, not a query_result. This is what you get
    when you do the following:

        stubhub_response = apiqueries.search_events(query='Timberwolves',
                groupingName='NBA Regular')
        events = stubhub_response.json()['events']

    """
    for event in events:
        if not event_exists(event['id']):
            dbsession.add(objectify_event(event))
    dbsession.commit()

def get_event_snapshots(event_id):
    """
    Returns a dict of event info which includes a dict from each listing
    snapshot associated with that event.
    The dict looks like this:
        {   'datetime': '2016-03-13 17:00 CDT',
        'event_name': 'Sacramento Kings vs. Utah Jazz',
        'snapshots': [   {   'avg_price': 32.453999999999994,
                             'datetime': '2016-01-27 13:03 CST',
                             'listing_prices': [   25.9,
                                                   26.29,
                                                   26.9,
                                                   26.9,
                                                   30.56,
                                                   36.66,
                                                   36.66,
                                                   37.73,
                                                   38.45,
                                                   38.49],
                             'snapshot_id': 9},
                         {   'avg_price': 32.453999999999994,
                             'datetime': '2016-01-27 13:35 CST',
                             'listing_prices': [   25.9,
                                                   26.29,
                                                   26.9,
                                                   26.9,
                                                   30.56,
                                                   36.66,
                                                   36.66,
                                                   37.73,
                                                   38.45,
                                                   38.49],
                             'snapshot_id': 19}]}
    """
    # get event from local db by stubhub event id
    event_results = dbsession.query(Event) \
              .filter(Event.stubhub_id == event_id).first()

    # get all listing snapshots associated with that event
    snapshot_results = dbsession.query(ListingSnapshot) \
              .filter(ListingSnapshot.event_id == event_results.id).all()

    # get a list of snapshot ids so we can get the listings associated to each
    # snapshot
    id_list = get_snapshot_id_list(snapshot_results)

    # get all listings associated with each snapshot
    listing_results = dbsession.query(Listing) \
              .filter(Listing.listing_snapshot_id.in_(id_list))

    event_datetime = timezones.utc_to_local(event_results.datetime)\
            .strftime("%Y-%m-%d %H:%M %Z")

    dict = {}
    dict['event_name'] = "%s vs. %s" % (event_results.team_primary,
            event_results.team_secondary)
    dict['datetime'] = event_datetime
    dict['snapshots'] = []
    for id in id_list:
        q = make_snapshot_data_dict(snapshot_results,listing_results,id)
        dict['snapshots'].append(q)
    return dict
