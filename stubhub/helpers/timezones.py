import pytz
from stubhub import settings

local_tz = pytz.timezone(settings.LOCAL_TIMEZONE)

def utc_to_local(utc_dt):
    local_dt = utc_dt.replace(tzinfo=pytz.utc).astimezone(local_tz)
    return local_tz.normalize(local_dt)

def as_utc(dt):
    return dt.astimezone(pytz.utc)
