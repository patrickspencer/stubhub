#!/usr/bin/env python
#-*- coding: utf-8 -*-

from setuptools import setup

readme = open('README.md').read()

setup(
    name = 'stubhub',
    version = '0.3',
    description = "A webapp for pulling tickets info from stubhub",
    long_description = readme,
    author = "Patrick Spencer",
    author_email = "pkspenc@gmail.com",
    url = "http://bitbucket.org/patrickspencer/stubhub/",
    py_modules = ['stubhub'],
    classifiers = [
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'Natural Language :: English',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7'
        'Programming Language :: Python :: 3.4'
    ],
    install_requires=[
        'requests == 2.9.1',
        'sqlalchemy == 1.0.11',
        'alembic == 0.8.4',
        'psycopg2 == 2.6.1',
        'numpy == 1.10.4',
        'pytz == 2015.7',
        'flask == 0.10.1',
        'sphinx == 1.3.5',
    ]
)
