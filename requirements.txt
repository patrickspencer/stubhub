# Install with: `pip install -r requirements.txt`

requests == 2.9.1
sqlalchemy == 1.0.11
alembic == 0.8.4
psycopg2 == 2.6.1
numpy == 1.10.4
pytz == 2015.7
flask == 0.10.1
sphinx == 1.3.5
gunicorn == 19.4.5

